<cfcomponent output="false">

	<cfscript>
		variables.instance = {};
		variables.instance.series = [];
		variables.instance.opts = { "grid":{ "hoverable":true, "clickable":true } };
		variables.instance.isPie = false;

		this.metadata = {
			attributetype = "fixed",
			attributes = {
				
				// attributes to mimic cfchart
				dataBackgroundColor={required:false,type:"any"},					// supports a simple hex/color value, or you can pass in an array, or array of structs confirming to Flot gradient definitions: see http://people.iola.dk/olau/flot/API.txt
				chartHeight={required:true,type:"numeric",hint:"Required"},							
				chartWidth={required:true,type:"numeric",hint:"Required"},
				foregroundColor={required:false,type:"string"},					
				gridlines={required:false,type:"any",default:""}, 					
				id={required:false,type:"string",default:"railoflot"}, 			
				labelFormat={required:false,type:"string",default:"number"}, 		// todo: number/currency/percent/date as in cfchart 
				scaleFrom={required:false,type:"numeric"},
				scaleTo={required:false,type:"numeric"},
				show3D={required:false,type:"boolean",default:true},				
				showBorder={required:false,type:"boolean",default:false},			
				showMarkers={required:false,type:"boolean",default:true},
				showXGridlines={required:false,type:"boolean",default:false},
				showYGridlines={required:false,type:"boolean",default:true},
				tipBGColor={required:false,type:"string",default:"white"},			
				tipStyle={required:false,type:"string",default:"MouseOver",hint:"MouseOver|MouseDown|none"}, 
				title={required:false,type:"string",default:""},					// todo:
				url={required:false,type:"string",default:""},						// todo:
				XAxisTitle={required:false,type:"string",default:""},				// todo:
				YAxisTitle={required:false,type:"string",default:""},				// todo:


				// these attributes are standard to cfchart, but value options have been extended for flot
				showLegend={required:false,type:"string",default:true},	 // boolean in cfchart, cfflot supports boolean or a string value of the jQuery object/DOM element/jQuery expression to place the legend into 

				// these attributes are extensions to the cfchart syntax (extras supported only by flot)
				XAxisLabels={required:false,type:"boolean",default:true,hint:"Display labels on the X-Axis?"},
				YAxisLabels={required:false,type:"boolean",default:true,hint:"Display labels on the Y-Axis?"},


				// these attributes are here to support cfchart syntax, but are currently ignored by cfflot
				// some are irrelevant and some just are too hard to implement right now based on what flot can do by itself
				backgroundColor={required:false,type:"string"},						
				format={required:false,type:"string"},						
				font={required:false,type:"string"},						
				fontBold={required:false,type:"boolean"},						
				fontItalic={required:false,type:"boolean"},						
				fontSize={required:false,type:"numeric"},						
				markerSize={required:false,type:"string"}, 		
				name={required:false,type:"string"},								
				pieSliceStyle={required:false,type:"string"},								
				seriesPlacement={required:false,type:"string"},								
				sortXAxis={required:false,type:"boolean",default:false},								
				style={required:false,type:"string"},								
				YOffset={required:false,type:"any"},					
				YAxisType={required:false,type:"any"},					
				XOffset={required:false,type:"any"},						
				XAxisType={required:false,type:"any"}						
			}
		};
	</cfscript>

	<cffunction name="init" access="public" output="false" returntype="void">
		<cfargument name="parent" type="component" required="false"/>
		<cfargument name="hasEndTag" type="boolean" required="true"/>
		
		<cfset variables.instance.hasEndTag = arguments.hasEndTag/>
		<cfreturn/>
	</cffunction>
	
	
	<cffunction name="onStartTag" access="public" output="true" returntype="boolean">
		<cfargument name="attributes" type="struct">
   		<cfargument name="caller" type="struct">

		<!--- generate a unique ID for this chart instance --->
		<cfset variables.instance.id = "_flot_" & attributes.id & "_" & randrange(1,9999999)/>
		<cfset variables.chartwidth = attributes.chartwidth/>
		
		<!--- set up the page with JS --->
		<cfsilent>
		<cfif not structkeyexists(request,"RailoFlotLoaded")>
			
			<!--- this script checks for the existance of jQuery and only adds it if it does not already exist --->
			<cfsavecontent variable="local.jqueryHead">
				<script type="text/javascript">
					if (typeof(jQuery)=="undefined") {
						var headID = document.getElementsByTagName("head")[0];         
						var newScript = document.createElement('script');
						newScript.type = 'text/javascript';
						newScript.src = '/mapping-tag/net/padawan/commons/html/libs/RailoJs.cfc?method=get&lib=jquery/jquery-1.3.2';
						headID.appendChild(newScript);
					}
				</script>
			</cfsavecontent>
			<cfhtmlhead text="#local.jqueryHead#"/>
			<cfsavecontent variable="local.flotHead">
				<cfoutput>
				<!--[if lte IE 8]><script language="javascript" type="text/javascript" src="/mapping-tag/net/padawan/commons/html/libs/RailoJs.cfc?method=get&lib=flot/excanvas"></script><![endif]-->
				<script language="javascript" type="text/javascript" src="/mapping-tag/net/padawan/commons/html/libs/RailoJs.cfc?method=get&lib=flot/jquery.flot"></script>
				<script language="javascript" type="text/javascript" src="/mapping-tag/net/padawan/commons/html/libs/RailoJs.cfc?method=get&lib=flot/jquery.flot.pie"></script>
				<cfif attributes.tipStyle neq "none">
				<script language="javascript" type="text/javascript" src="/mapping-tag/net/padawan/commons/html/libs/RailoJs.cfc?method=get&lib=flot/railo.flot.extras"></script>
				</cfif>
				</cfoutput>
			</cfsavecontent>
			<cfhtmlhead text="#local.flotHead#"/>
		</cfif>

		<cfset request.RailoFlotLoaded = true/>
		</cfsilent>
		
		
		<!--- set some Flot properties --->

		<!--- data background color --->
		<cfif structkeyexists(attributes,"dataBackgroundColor")>
			<cfif isSimpleValue(attributes.dataBackgroundColor)>
				<cfset variables.instance.opts["grid"]["backgroundColor"]=attributes.dataBackgroundColor/>
			<cfelseif isArray(attributes.dataBackgroundColor)>
				<cfset variables.instance.opts["grid"]["backgroundColor"]["colors"]=attributes.dataBackgroundColor/>
			</cfif>
		</cfif>	

		<!--- foreground color --->
		<cfif structkeyexists(attributes,"foregroundColor")>
			<cfset variables.instance.opts["grid"]["color"]=attributes.foregroundColor/>
		</cfif>	

		<!--- shadows? --->
		<cfif not attributes.show3D>
			<cfset variables.instance.opts["series"]["shadowSize"]=val(0)/>
		</cfif>	
				
		<!--- show legend? --->
		<cfif isBoolean( attributes.showLegend )>
			<cfset variables.instance.opts["legend"]={ "show":#attributes.showLegend ? true : false# }>
		<cfelse>
			<cfset variables.instance.opts["legend"]={ "show":true, "container":"#attributes.showlegend#" }>
		</cfif>

		<!--- show markers? --->
		<cfset variables.instance.opts["series"]["points"]={ "show":#attributes.showmarkers ? true : false# }>
		
		<!--- number of gridlines on value axis (ala CFCHART)? --->
		<cfif isnumeric(attributes.gridlines)>
			<cfset variables.instance.opts["yaxis"]["ticks"]=val(attributes.gridlines)/>
		</cfif>
		
		<!--- show gridlines? FLot only supports all or none, unlike cfchart which allows turning on and off X and Y individually. So if either are specified to be shown, we'll show both --->
		<cfif not attributes.showXGridLines and not attributes.showYGridlines>
			<cfset variables.instance.opts["grid"]["tickColor"]="transparent"/>
		</cfif>

		<!--- min / max values --->
		<cfif structkeyexists(attributes,"scaleFrom")>
			<cfset variables.instance.opts["yaxis"]["min"]=val(attributes.scaleFrom)/>
		</cfif>
		<cfif structkeyexists(attributes,"scaleTo")>
			<cfset variables.instance.opts["yaxis"]["max"]=val(attributes.scaleTo)/>
		</cfif>
		
		<cfset variables.instance.tipstyle = attributes.tipstyle>
		
		<!--- display labels? --->
		<cfif not attributes.XAxisLabels>
			<cfset variables.instance.opts["xaxis"]["labelWidth"]=0/>
			<cfset variables.instance.opts["xaxis"]["labelHeight"]=0/>
		</cfif>
		<cfif not attributes.YAxisLabels>
			<cfset variables.instance.opts["yaxis"]["labelWidth"]=0/>
			<cfset variables.instance.opts["yaxis"]["labelHeight"]=0/>
		</cfif>
				
		
		<cfreturn variables.instance.hasEndTag/>
	</cffunction>
	
	
	<cffunction name="onEndTag" access="public" returntype="boolean" output="true">
		<cfargument name="attributes" type="struct">
   		<cfargument name="caller" type="struct">

		<!--- output placeholder --->
		<cfif attributes.showBorder>
			<div style="padding:10px 20px; border:1px solid black;">
		</cfif>
		<cfoutput><div id="#variables.instance.id#" style="width:#attributes.showBorder ? attributes.chartwidth-40 : attributes.chartwidth#px; height:#attributes.showBorder ? attributes.chartheight-20 : attributes.chartheight#px;"></div></cfoutput>
		<cfif attributes.showBorder>
			</div>
		</cfif>

		<!--- output javascript --->
		<cfoutput>
			<script type="text/javascript">
				$(function(){
					var opts 	= #serializeJSON(variables.instance.opts)#;
					var series 	= #serializeJSON(variables.instance.series)#;

					<cfif not attributes.xAxisLabels>
						opts['xaxis']['tickFormatter']=RailoFlotUtil.suppressLabels;
					</cfif>
					<cfif not attributes.yAxisLabels>
						opts['yaxis']['tickFormatter']=RailoFlotUtil.suppressLabels;
					</cfif>
					
					plot_#variables.instance.id# = jQuery.plot('###variables.instance.id#', series, opts );
					extras_#variables.instance.id# = new RailoFlotExtras("#attributes.tipBGColor#","#attributes.tipStyle eq 'MouseOver' ? 'plothover' : 'plotclick'#",plot_#variables.instance.id#);
					<cfif variables.instance.tipstyle neq "none">
						jQuery("###variables.instance.id#").bind(extras_#variables.instance.id#.bind, extras_#variables.instance.id#.fnBind);
					</cfif>
				});
			</script>
		</cfoutput>
	
		<cfreturn false/>
	</cffunction>
	
	
	<cffunction name="appendSeries" access="public" returntype="void" output="false">
		<cfargument name="series" type="struct" required="true"/>
		<cfset arrayappend(variables.instance.series,arguments.series)/>
		<cfreturn/>
	</cffunction>


	<cffunction name="setAxisFormat" access="public" returntype="void" output="false">
		<cfargument name="axis" type="string" required="true"/>
		<cfargument name="format" type="string" required="true"/>
		
		<cfif listfirst(arguments.format,":") eq "date" or listfirst(arguments.format,":") eq "time">
			<cfset variables.instance.opts["#arguments.axis#axis"]["mode"]="time"/>
			<cfset variables.instance.opts["#arguments.axis#axis"]["timeformat"]=listlast(arguments.format,":")/>
		</cfif>
		
		<cfreturn/>
	</cffunction>


	<cffunction name="setAxisMargin" access="public" returntype="void" output="false">
		<cfargument name="axis" type="string" required="true"/>
		<cfargument name="margin" type="numeric" required="true"/>
		
		<cfset variables.instance.opts["#arguments.axis#axis"]["autoscaleMargin"]=val(arguments.margin)/>
		<cfreturn/>
	</cffunction>

	<cffunction name="setPadding" access="public" returntype="void" output="false">
		<cfargument name="top" type="numeric" required="false" default="0"/>
		<cfargument name="right" type="numeric" required="false" default="0"/>
		<cfargument name="bottom" type="numeric" required="false" default="0"/>
		<cfargument name="left" type="numeric" required="false" default="0"/>
		
		<cfset variables.instance.opts["plotOffset"]={ "left":arguments.left, "right":arguments.right, "top":arguments.top, "bottom":arguments.bottom }/>
		<cfreturn/>
	</cffunction>
	
	
	<cffunction name="setPie" access="public" returntype="void">
		<cfset variables.instance.opts["series"]["pie"] = { "show":true, "radius":4/5, "label":{ "show":false } }/> 
		<!--- todo: support tooltips on pie charts, currently they cause JS errors, I think due to non-integer label values --->
		<cfset variables.instance.tipstyle = "none">
		<cfreturn>
	</cffunction>
		
	<cffunction name="getShowLegend" access="public" returntype="boolean" output="false">
		<cfreturn variables.instance.opts.legend.show/>
	</cffunction>

	<cffunction name="getShowMarkers" access="public" returntype="boolean" output="false">
		<cfreturn variables.instance.opts.series.points.show/>
	</cffunction>
	
	<cffunction name="getChartWidth" access="public" returntype="numeric" output="false">
		<cfreturn variables.chartwidth/>
	</cffunction>	

</cfcomponent>