<cfcomponent output="false">

	<cfscript>
		variables.instance = {};
		variables.instance._SUPPORTED_TYPES = "line,area,bar,step,scatter,curve,horizontalbar,pie";
		variables.instance.isPie = false;
		
		this.metadata = {
			attributetype = "fixed",
			attributes = {
				
				// attributes to mimic cfchartseries
				type={required:false,type:"string",default:"line",hint:"line|bar|step|scatter|curve|horizontalbar|pie"},  // not implemented from cfchart: pyramid, area, cone, cylinder, pie, (curve is implemented as line)
				itemColumn={required:false,type:"string"},							
				itemFormat={required:false,type:"string"},												// you can use date:[flot date mask] here for date formatting ex: itemformat="date:%b %d" 
																										/*
																										 *   	%h: hours
																											  	%H: hours (left-padded with a zero)
																											  	%M: minutes (left-padded with a zero)
																											  	%S: seconds (left-padded with a zero)
																											  	%d: day of month (1-31)
																											  	%m: month (1-12)
																											  	%y: year (four digits)
																											  	%b: month name (customizable)
																											  	%p: am/pm, additionally switches %h/%H to 12 hour instead of 24
																											  	%P: AM/PM (uppercase version of %p)
																										 **/
				valueColumn={required:false,type:"string"},
				valueFormat={required:false,type:"string"},
				query={required:false,type:"string"},
				seriesColor={required:false,type:"string",default:""},
				seriesLabel={required:false,type:"string",default:""},

				// currently only applies to pie charts
				colorlist={required:false,type:"any",default:""}, 										// list of colors for pie elements. default flot colors are used when this list is exhausted. pass without hashes: i.e. 000000,FFFFFF,E0E0E0
				dataLabelStyle={required:false,type:"any",default:"none"}, 								// how to dispay labels on pie elements: none|value|rowLabel|columnLabel|pattern
				
				
				// attributes to support cfchartseries which are currently ignored by Flot
				markerStyle={required:false,type:"any",default:""}, 									// not implemented yet
				paintStyle={required:false,type:"any",default:""} 										// not implemented yet
			}
		};
	</cfscript>

	
	<cffunction name="init" access="public" output="false" returntype="void">
		<cfargument name="parent" type="component" required="false"/>
		<cfargument name="hasEndTag" type="boolean" required="true"/>
		
		<cfset variables.parent = arguments.parent/>
		<cfset variables.instance.hasEndTag = arguments.hasEndTag/>
		<cfreturn/>
	</cffunction>
	
	
	
	<cffunction name="onStartTag" access="public" output="false" returntype="boolean">
		<cfargument name="attributes" type="struct">
   		<cfargument name="caller" type="struct">

		<!--- extra attribute validation (combinations) --->
		<cfif structkeyexists(attributes,"query") and (not structkeyexists(attributes,"itemColumn") or not structkeyexists(attributes,"valueColumn"))>
			<cfthrow message="[query] attribute requires both [itemcolumn] and [valuecolumn] attributes"/>
		</cfif>		
		<!--- extra attribute validation (type) --->
		<cfif not listfindnocase(variables.instance._SUPPORTED_TYPES,attributes.type)>
			<cfthrow message="type #attributes.type# is not currently supported by [cfflotseries]"/>
		</cfif>		
		<!--- extra attribute validation (makes sure query is a query) --->
		<cfif structkeyexists(attributes,"query")>
			<cfif not structkeyexists(caller,attributes.query) or not isQuery(evaluate("caller." & attributes.query))>
				<cfthrow message="[query] attribute must be the name of a valid query."/>
			<cfelse>
				<cfset attributes.query = evaluate("caller." & attributes.query)/>
			</cfif>
		</cfif>		
		
		<cfset this.setType(attributes.type)/>

		<!--- set up the series object --->
		<cfset variables.instance.series = {
			"color":"#attributes.seriesColor#",
		    "data":[],
		    "label":"#attributes.seriesLabel#"
		}/>

		
		<!--- set flot options based on series type --->
		<cfif attributes.type eq "line" or attributes.type eq "curve">
			<cfset variables.instance.series["lines"] = { "show":true }/>
		<cfelseif attributes.type eq "step">
			<cfset variables.instance.series["lines"] = { "show":true, "steps":true }/>
		<cfelseif attributes.type eq "scatter">
			<cfset variables.instance.series["points"] = { "show":true }/>
			<cfset variables.instance.series["lines"] 	= { "show":false }/>
		<cfelseif attributes.type eq "bar">
			<cfset local.bargradient = this.CalculateBarGradient( attributes.seriescolor )>
			<cfset variables.instance.series["color"] = local.bargradient[1]/> 
			<cfset variables.instance.series["bars"] = { "show":true, "align":"center", "fill":1, "lineWidth":1, "fillColor":{ "colors": local.bargradient } }/> 
			<cfset variables.instance.series["lines"] 	= { "show":false }/>
			<cfset variables.instance.series["points"] 	= { "show":false }/>
			<!--- so that first and last bars are off the chart edges --->
			<cfset this.getParent().setAxisMargin("x","0.00")/>
		<cfelseif attributes.type eq "horizontalbar">
			<cfset variables.instance.series["bars"] = { "show":true, "horizontal":true, "fill":1, "lineWidth":1 }/>
			<cfset variables.instance.series["lines"] 	= { "show":false }/>
			<cfset variables.instance.series["points"] 	= { "show":false }/>
		<cfelseif attributes.type eq "pie">
			<cfset this.getParent().setPie()>
			<cfset variables.instance.isPie = true>
		</cfif>
		
		<!--- show markers or not is determined in parent tag <cfflot> --->
		<cfif this.getParent().getShowMarkers() and listfindnocase("line,step,curve",attributes.type)>
			<cfset variables.instance.series["points"]={ "show":true }/>
		</cfif>

		<cfif structkeyexists(attributes,"itemFormat")>
			<cfset this.getParent().setAxisFormat("x",attributes.itemFormat)/>
		</cfif>
		<cfif structkeyexists(attributes,"valueFormat")>
			<cfset this.getParent().setAxisFormat("y",attributes.valueFormat)/>
		</cfif>
		
		<!--- if we have a query attribute, build the raw data --->
		<cfif structkeyexists(attributes,"query")>
			<cfscript>
				local.flotdata = createobject("component","flotdata").initFromSeries(parent=this,hasEndTag="false");
			</cfscript>
			<cfloop query="attributes.query">
				<cfset local.flotdataCollection = { item=evaluate( "attributes.query." & attributes.itemColumn ), value=evaluate("attributes.query." & attributes.valueColumn ) }/>
				<cfset local.flotdata.onStartTag(attributes=local.flotdataCollection,caller=variables)/>
			</cfloop>
			<cfreturn this.onEndTag(attributes,caller)/>	
		</cfif>
		
		<cfreturn variables.instance.hasEndTag/>		
	</cffunction>
	
	
	<cffunction name="onEndTag" access="public" output="false" returntype="boolean">
		<cfargument name="attributes" type="struct">
   		<cfargument name="caller" type="struct">

		<cfif this.getType() eq "bar" or this.getType() eq "horizontalbar">
			<cfset variables.instance.series["bars"]["barWidth"]=val(this.getBarWidth())/>
		</cfif>	

		<cfif variables.instance.isPie>
			<!--- convert series data to pie format --->
			<cfscript>
				local.data = variables.instance.series.data;
				for (local.ii=1;local.ii lte arraylen(local.data);local.ii++) {
					local.slice = { "label":local.data[local.ii][1], "data":local.data[local.ii][2] };
					if ( listlen(attributes.colorlist) gte local.ii ) {
						local.slice["color"] = "###listgetat( attributes.colorList, local.ii)#";	
					}
					variables.parent.appendSeries( local.slice );	
				}
			</cfscript>
			<cfreturn false>
		</cfif>
		
		<cfset variables.parent.appendSeries(variables.instance.series)/>	
		<cfreturn false>
	</cffunction>

	<cffunction name="appendData" access="public" returntype="void" output="false">
		<cfargument name="data" type="array" required="true"/>
		
		<cfset arrayappend(variables.instance.series.data,arguments.data)/>
		<cfreturn/>
	</cffunction>
	
	<cffunction name="getParent" access="public" returntype="component" output="false">
		<cfreturn variables.parent/>
	</cffunction>

	<cffunction name="getBarWidth" access="public" returntype="numeric" output="false">
		<cfset var local = {}/>
		
		<cfscript>
			local.seriesdata = variables.instance.series.data;
			
			if (this.getType() neq "horizontalbar") {
				//local.chartwidth 	= this.getParent().getChartWidth();
				local.itemrange 	= local.seriesdata[ arraylen(local.seriesdata) ][1] - local.seriesdata[1][1];
				local.itemcount 	= arraylen(local.seriesdata);
				
				// how wide is each item on the graph?
				local.itemwidth 	= local.itemrange / local.itemcount;
				
				// set barwidth to 75% of that
				return val(local.itemwidth*0.75);
			} else {
			// todo: base on chart height instead
			}
		</cfscript>
		
		<cfreturn 1/>
	</cffunction>

	<cffunction name="setType" access="public" returntype="void" output="false">
		<cfargument name="seriestype" type="string" required="true"/>
		<cfset variables.seriestype = arguments.seriestype>
		<cfreturn/>
	</cffunction>
	
	<cffunction name="getType" access="public" returntype="string" output="false">
		<cfreturn variables.seriestype/>
	</cffunction>
	
	<cffunction name="NormalizeColor" access="public" returntype="struct" output="false" hint="Take a HEX value or R,G,B[,A] list and creates a normalized color structure that can be safely used by the other functions of this utility component.">
		<cfargument name="Color" type="any" required="true" hint="A HEX or R,G,B[,A] color list or struct."/>
			
		<!--- Define the local scope. --->
		<cfset var LOCAL = {} />
		
		<!--- 
			Create the default color object. This will get returned if the 
			passed-in color cannot be normalized.
		--->
		<cfset LOCAL.Return = {
			Red = 255,
			Green = 255,
			Blue = 255,
			Alpha = 255,
			Hex = "##FFFFFF"
			} />
		
		
		<!--- Check to see if the passed in value is a struct. --->
		<cfif IsStruct( ARGUMENTS.Color )>
		
			<!--- The passed in color was a struct. Let's see if we can copy over the R,G,B,A values. --->
			<cfloop
				index="LOCAL.Key"
				list="Red,Green,Blue,Alpha"
				delimiters=",">
					
				<cfif StructKeyExists( ARGUMENTS.Color, LOCAL.Key )>
				
					<!--- Copy this key to return color. --->
					<cfset LOCAL.Return[ LOCAL.Key ] = ARGUMENTS.Color[ LOCAL.Key ] />
				
				</cfif>
				
			</cfloop>		
		
		<!--- Check to see if we are dealing with a HEX value or an R,G,B list. --->
		<cfelseif REFind( "(?i)^##?[0-9A-F]{6}$", ARGUMENTS.Color )>
		
			<!--- Strip out the non-hex characters values. --->
			<cfset ARGUMENTS.Color = REReplace(
				ARGUMENTS.Color,
				"(?i)[^0-9A-F]+",
				"",
				"all"
				) />
				
			<!--- Get the decimal value of this hex. --->
			<cfset LOCAL.DecimalColor = InputBaseN( ARGUMENTS.Color, "16" ) />
			
			<!--- Set the red channel. --->
			<cfset LOCAL.Return.Red = BitSHRN(
					BitAnd( LOCAL.DecimalColor, InputBaseN( "FF0000", 16 ) ),
					16
					) />
					
			<!--- Set the green channel. --->
			<cfset LOCAL.Return.Green = BitSHRN(
					BitAnd( LOCAL.DecimalColor, InputBaseN( "00FF00", 16 ) ),
					8
					) />
				
			<!--- Set the blue channel. --->
			<cfset LOCAL.Return.Blue = BitAnd( LOCAL.DecimalColor, InputBaseN( "0000FF", 16 ) ) />
				
		<!--- Check to see if we are dealing with a list of R,G,B colors. --->
		<cfelseif REFind( "^\d+,\d+,\d+$", ARGUMENTS.Color )>
		
			<!--- Store the channels. --->
			<cfset LOCAL.Return.Red = ListGetAt( ARGUMENTS.Color, 1 ) />
			<cfset LOCAL.Return.Green = ListGetAt( ARGUMENTS.Color, 2 ) />
			<cfset LOCAL.Return.Blue = ListGetAt( ARGUMENTS.Color, 3 ) />
			
		<!--- Check to see if we are dealing with a list of R,G,B,A colors. --->
		<cfelseif REFind( "^\d+,\d+,\d+,\d+$", ARGUMENTS.Color )>
		
			<!--- Store the channels. --->
			<cfset LOCAL.Return.Red = ListGetAt( ARGUMENTS.Color, 1 ) />
			<cfset LOCAL.Return.Green = ListGetAt( ARGUMENTS.Color, 2 ) />
			<cfset LOCAL.Return.Blue = ListGetAt( ARGUMENTS.Color, 3 ) />
			<cfset LOCAL.Return.Alpha = ListGetAt( ARGUMENTS.Color, 4 ) />
		
		</cfif>
		
		
		<!--- Set the HEX value based on the RGB. --->
		<cfset LOCAL.Return.Hex = UCase(
			Right( "0#FormatBaseN( LOCAL.Return.Red, '16' )#", 2 ) &
			Right( "0#FormatBaseN( LOCAL.Return.Green, '16' )#", 2 ) &
			Right( "0#FormatBaseN( LOCAL.Return.Blue, '16' )#", 2 )
			) />
			
		<!--- Return RGB color. --->
		<cfreturn LOCAL.Return />	
		
	</cffunction>

	<cffunction name="CalculateBarGradient" access="public" returntype="array" output="false" hint="Given a From and To normalized color structure (as defined by the NormalizeColor() function) that contain Red, Green, Blue, and Alpha keys, it will return the equivalent structs for each step of the gradient.">
		
		<!--- 
			this is based on Ben Nadel's function CalculateGradient from the imageUtils.cfc project  
			I modified to take pure string inputs and to also construct the gradient to match Adobe 
			ColdFUsion flash charts bar gradient style: the gradient begins half way up the bar and 
			fades only mostly to black - not entirely.
			
			the array it returns is also different from Ben's. His returns the normalized color structure
			whereas this returns string values in the form "rgba(R,G,B,A)"
		--->
		
			
		<cfargument
			name="ToColor"
			type="string"
			required="true"
			/>
			
		<cfargument
			name="FromColor"
			type="string"
			required="false"
			default="000000"
			/>

		<cfargument
			name="Steps"
			type="numeric"
			required="false"
			default="10"
			hint="The number of steps overwhich to calculate the gradient."
			/>
			
		<!--- Define the local scope. --->
		<cfset var LOCAL = {} />
		
		<!--- normalize the color inputs --->
		<cfset arguments.fromcolor = this.normalizecolor(arguments.fromcolor)/>
		<cfset arguments.tocolor = this.normalizecolor(arguments.tocolor)/>
		
		<!--- 
			Find the differences between the two and from colors. 
			We will getting this by finding the difference of each 
			color chanel in RGB format. 
		--->
		<cfset LOCAL.RedDelta = (ARGUMENTS.ToColor.Red - ARGUMENTS.FromColor.Red) />
		<cfset LOCAL.GreenDelta = (ARGUMENTS.ToColor.Green - ARGUMENTS.FromColor.Green) />
		<cfset LOCAL.BlueDelta = (ARGUMENTS.ToColor.Blue - ARGUMENTS.FromColor.Blue) />
		<cfset LOCAL.AlphaDelta = (ARGUMENTS.ToColor.Alpha - ARGUMENTS.FromColor.Alpha) />
		
		<!--- 
			Based on the number of steps that we want to define the 
			gradient, find the step for each color delta.
		--->
		<cfset LOCAL.RedStep = (LOCAL.RedDelta / ARGUMENTS.Steps) />
		<cfset LOCAL.GreenStep = (LOCAL.GreenDelta / ARGUMENTS.Steps) />
		<cfset LOCAL.BlueStep = (LOCAL.BlueDelta / ARGUMENTS.Steps) />
		<cfset LOCAL.AlphaStep = (LOCAL.AlphaDelta / ARGUMENTS.Steps) />
		
		
		<!--- Create an array to hold the color steps. --->
		<cfset LOCAL.Gradient = [] />
		
		<!--- Create a start color. --->
		<cfset LOCAL.Color = "rgba( #arguments.FromColor.red#, #arguments.fromCOlor.green#, #arguments.fromcolor.blue#, #arguments.fromcolor.alpha# )" />
		
		<!--- Loop over color differences to calculate. --->
		<cfloop
			index="LOCAL.StepIndex"
			from="1"
			to="#ARGUMENTS.Steps#"
			step="1">
			
			<!--- Store the gradient step. --->
			<cfset ArrayAppend(
				LOCAL.Gradient,
				local.color
				) />
			
			<!--- 
				Increment color. In order to make sure that the 
				gradient steps get used appropriatly, add the steps 
				directly the FROM color rather than to the previous 
				color index. This will prevent the Fix() function 
				from stopping our gradient if the increment is too 
				small.
			--->
			<cfset Local.Color = "rgba( #Fix( ARGUMENTS.FromColor.Red + (LOCAL.RedStep * LOCAL.StepIndex) )#, #Fix( ARGUMENTS.FromColor.Green + (LOCAL.GreenStep * LOCAL.StepIndex) )#, #Fix( ARGUMENTS.FromColor.Blue + (LOCAL.BlueStep * LOCAL.StepIndex) )#, #Fix( ARGUMENTS.FromColor.Alpha + (LOCAL.AlphaStep * LOCAL.StepIndex) )# )">
			
		</cfloop>
		
		<!--- we want the bar gradient to begin halfway up the bar, so append the final color into the array again, according to number of steps less 3 --->
		<cfloop from="1" to="#arguments.steps-2#" index="Local.stepIndex">
			<cfset ArrayAppend(
				LOCAL.Gradient,
				local.color
				) />
		</cfloop>
		
		<!--- now trim off the first three elements of the gradient array (so it doesn't begin at black) --->
		<cfloop from="1" to="5" index="Local.stepindex">
			<cfset arraydeleteat(Local.Gradient,1)/>
		</cfloop>
		
		
		<!--- Return gradient array. --->
		<cfreturn LOCAL.Gradient />
	</cffunction>

</cfcomponent>