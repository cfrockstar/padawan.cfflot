function RailoFlotExtras(bgcolor,bind,plot) {
	
	this.bgcolor=bgcolor;
	this.bind=bind;
	this.plot=plot;
	this.previousPoint=null;
	
	var self = this;
		
	// ////////////////////////////////////////////////////////////////////////////////////////
	//
	//			functions for doing MouseHover and MouseClick tooltips
	//
	this.showCoordinateData = function(x,y,contents) {
		jQuery('<div id="flotCoordinateData">' + contents + '</div>').css( {
            position: 'absolute',
            display: 'none',
            top: y - 20,
            left: x + 20,
            border: '1px solid #000',
            padding: '5px 10px',
            'background-color': self.bgcolor,
            opacity: 0.80,
			'font-size':'8pt'
        }).appendTo("body").show();
	};
	
	this.fnBind = function(event,pos,item) {
		
		if (item) {
			if (self.previousPoint != item.datapoint) {
				self.previousPoint = item.datapoint;
				
				jQuery("#flotCoordinateData").remove();
				var x = item.datapoint[0].toFixed(2), y = item.datapoint[1].toFixed(2);
				
				if ( self.plot.getOptions().xaxis.mode=='time' ) {
					xDate = new Date(item.datapoint[0]);
					x = jQuery.plot.formatDate(xDate, self.plot.getOptions().xaxis.timeformat, self.plot.getOptions().xaxis.monthNames);
				}
				if ( self.plot.getOptions().yaxis.mode=='time' ) {
					console.log("formatting y");
					y = jQuery.plot.formatDate(Date(item.datapoint[1]), self.plot.getOptions().yaxis.timeformat, self.plot.getOptions().yaxis.monthNames);
				}
				
				self.showCoordinateData(item.pageX, item.pageY, x + "<br>" + y);
			}
		}
		else {
			jQuery("#flotCoordinateData").remove();
			self.previousPoint = null;
		}
	}
	//
	//
	// ////////////////////////////////////////////////////////////////////////////////////////
};


var RailoFlotUtil = {

	// ////////////////////////////////////////////////////////////////////////////////////////
	//
	//			functions for formatting labels
	//
	suppressLabels:function(val, axis) {
		return '';
	},	
	//
	//
	// ////////////////////////////////////////////////////////////////////////////////////////

	// ////////////////////////////////////////////////////////////////////////////////////////
	//
	//			function for inverting axis
	//
	invertAxis:function(v) {
		return -v;
	}	
	//
	//
	// ////////////////////////////////////////////////////////////////////////////////////////

}

					

