<cfcomponent output="false">

	<cfscript>
		variables.instance = {};
		
		this.metadata = {
			attributetype = "fixed",
			attributes = {
				// attributes to mimic cfchartdata
				item={required:true,type:"any",hint:"X axis value"},  
				value={required:true,type:"any",hint:"Y axis value"}
			}
		};
	</cfscript>

	
	<cffunction name="init" access="public" output="false" returntype="void">
		<cfargument name="parent" type="component" required="false"/>
		<cfargument name="hasEndTag" type="boolean" required="true"/>

		<cfset variables.parent = arguments.parent/>
		<cfreturn/>
	</cffunction>

	<cffunction name="initFromSeries" access="public" output="false" returntype="component">
		<cfargument name="parent" type="component" required="false"/>
		<cfargument name="hasEndTag" type="boolean" required="true"/>

		<cfset this.init(argumentCollection=arguments)/>
		
		<cfreturn this/>
	</cffunction>

	<cffunction name="onStartTag" access="public" returntype="boolean" output="false">
		<cfargument name="attributes" type="struct">
   		<cfargument name="caller" type="struct">

		<!--- todo: why did I have this restriction before? tooltip errors? --->
		<!--- <cfif not isnumeric(attributes.item) and not isdate(attributes.item)>
			<cfthrow message="[cfflot] currently only supports plotting datetime and numeric values"/>
		</cfif> --->
		<cfif not isnumeric(attributes.value) and not isdate(attributes.value)>
			<cfthrow message="[cfflot] currently only supports plotting datetime and numeric values"/>
		</cfif>

		<cfif isdate(attributes.item)>
			<cfset attributes.item = this.jsTimeformat(attributes.item)/>
		</cfif>
		
		<cfif isdate(attributes.value)>
			<cfset attributes.value = this.jsTimeformat(attributes.value)/>
		</cfif>

		<cfif this.getParent().getType() neq "horizontalbar">
		<!--- normal data format --->
			<cfset this.getParent().appendData( [ attributes.item, attributes.value ] )/>
		<cfelse>
		<!--- reversed data format for horizontal bars --->
			<cfset this.getParent().appendData( [ attributes.value, attributes.item ] )/>
		</cfif>

		<cfreturn false/>		
	</cffunction>

	<cffunction name="getParent" access="public" returntype="component" output="false">
		<cfreturn variables.parent/>
	</cffunction>
	
	
	<cffunction name="jsTimeFormat" access="private" returntype="numeric" output="false" hint="Converts datetime to Javascript Time Format (required by flot for timeseries)">
		<cfargument name="date" type="date" required="true"/>
		<cfset local = {}/>
		<!--- Flot dates are always UTC in Javascript Time Format (milliseconds since 1/1/1970) --->
		
		<!--- convert to UTC --->
		<cfset local.date = dateconvert("local2Utc",arguments.date)/>
		
		<!--- return seconds between date and 1/1/1970 * 1000 --->
		<cfreturn datediff("s",createdate(1970,1,1),local.date)*1000/>
	</cffunction>


</cfcomponent>